{-# LANGUAGE StandaloneDeriving, DeriveDataTypeable, TypeSynonymInstances,MultiParamTypeClasses #-}
-- | Module for using HUnit tests
module Test.MuCheck.TestAdapter.HUnit where
import qualified Test.HUnit as HUnit
import Test.MuCheck.TestAdapter

import Data.Typeable

deriving instance Typeable HUnit.Counts
type HUnitSummary = HUnit.Counts

-- | Summarizable instance of `HUnitSumary`
instance Summarizable HUnitSummary where
  testSummary _mutant _test result = Summary $ _ioLog result
  isSuccess c = (HUnit.cases c == HUnit.tried c) && HUnit.failures c == 0 && HUnit.errors c == 0
  isFailure c = HUnit.failures c > 0

data HUnitRun = HUnitRun String

instance TRun HUnitRun HUnitSummary where
  genTest _m tstfn = "runTestTT " ++ tstfn
  getName (HUnitRun str) = str
  toRun s = HUnitRun s

  summarize_ _m = testSummary :: Mutant -> TestStr -> InterpreterOutput HUnitSummary -> Summary
  success_ _m = isSuccess :: HUnitSummary -> Bool
  failure_ _m = isFailure :: HUnitSummary -> Bool
  other_ _m = isOther :: HUnitSummary -> Bool

