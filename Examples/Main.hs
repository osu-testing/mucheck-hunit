module Main where
import Test.HUnit
import Examples.HUnitTest

main = do
  runTestTT test1
  runTestTT test2
  runTestTT test3
  return 0
